# Ceres solver tutorial

Solving non-linear least squares using Ceres Solver API:
- [CeresTuto] - HTML enhanced for web apps!

<!-- vim-markdown-toc GitLab -->

* [Installation](#installation)
    * [Build](#build)
    * [Create Conan package](#create-conan-package)
* [Tutorial problems](#algorithm)

<!-- vim-markdown-toc -->

## Installation

### Build

To build this project you must have `conan`, `cmake` and a compiler compliant with cpp17.

```bash
git clone git@gitlab.com:cpp-training/ceres_tuto.git
mkdir build
cd build
conan install ../ceres_tuto -s compiler.cppstd=17 -b missing
cmake ../ceres_tuto
make
ctest
```

### Create Conan package

```bash
conan create . -s compiler.cppstd=17 -b missing
```

## Tutorial problems



[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [CeresTuto]: <http://ceres-solver.org/nnls_tutorial.html>





