#!/bin/bash
new_name=""

while [ "$new_name" = "" ]
do
    read -p "Please set a new name for this project : " new_name
    case "$new_name" in  
     *\ * )
           echo "The name shoud not contain whitespace."
           new_name=""
          ;;
    esac
done

actual_name=$(ls -d modules/*/ | cut -f2 -d'/')
nbModule=$(echo $actual_name | wc -w)
if [ $nbModule != "1" ]
then
    echo "Something wrong. Too many directories in modules."
    exit 0;
fi

#replace the project name in the first CMakeLists.txt
sed -i 's/'$actual_name'/'$new_name'/Ig' CMakeLists.txt

new_name_upper_case=$(echo $new_name | tr '[:lower:]' '[:upper:]')
new_name_lower_case=$(echo $new_name | tr '[:upper:]' '[:lower:]')
actual_name_upper_case=$(echo $actual_name | tr '[:lower:]' '[:upper:]')
actual_name_lower_case=$(echo $actual_name | tr '[:upper:]' '[:lower:]')

find modules -type f -exec sed -i 's/'$actual_name_upper_case'/'$new_name_upper_case'/g' {} +
find modules -type f -exec sed -i 's/'$actual_name_lower_case'/'$new_name_lower_case'/g' {} +
find app -type f -exec sed -i 's/'$actual_name_upper_case'/'$new_name_upper_case'/g' {} +
find app -type f -exec sed -i 's/'$actual_name_lower_case'/'$new_name_lower_case'/g' {} +
find tests -type f -exec sed -i 's/'$actual_name_upper_case'/'$new_name_upper_case'/g' {} +
find tests -type f -exec sed -i 's/'$actual_name_lower_case'/'$new_name_lower_case'/g' {} +

mv modules/$actual_name modules/$new_name_lower_case