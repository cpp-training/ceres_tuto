#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <numeric>

#include <ceres_solvers/unconstrained/derivativeenum.h>

#include <ceres_solvers/unconstrained/easy_peasy.h>
#include <ceres_solvers/unconstrained/curve_fitting.h>
#include <ceres_solvers/unconstrained/ratkowsky3.h>
#include <ceres_solvers/unconstrained/rosenbrock.h>

template<typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec)
{
    for (auto& el : vec)
    {
        os << el << ' ';
    }
    return os;
}


int main()
{
    std::vector<std::string> vec = {
        "Hello", "from", "GCC", __VERSION__, "!" 
    };
    std::cout << vec << std::endl;

    std::cout << "-------------------- EASY PEASY -------------------" << std::endl ;
    minimize_easy_peasy(derivative::automatic);
    std::cout << "---------------------------------------------------" << std::endl ;
    minimize_easy_peasy(derivative::analytic);
    std::cout << std::endl ;

    std::cout << "-------------------- CURVE FITTING -------------------" << std::endl ;
    solve_curve_fitting(derivative::automatic);
    std::cout << "---------------------------------------------------" << std::endl ;
    solve_curve_fitting(derivative::analytic);
    std::cout << std::endl ;

    std::cout << "-------------------- RATKOWSKY -------------------" << std::endl ;
    solve_ratkowsky_fitting(derivative::automatic);
    std::cout << "---------------------------------------------------" << std::endl ;
    solve_ratkowsky_fitting(derivative::analytic);
    std::cout << std::endl ;

    std::cout << "-------------------- ROSENBROCK -------------------" << std::endl ;
    minimize_rosenbrock(derivative::analytic);
    std::cout << std::endl ;
}
