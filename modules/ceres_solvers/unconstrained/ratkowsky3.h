#ifndef RATKOWSKY3_H
#define RATKOWSKY3_H

#include <ceres/ceres.h>

#include "derivativeenum.h"

// Problem data is there
// https://www.itl.nist.gov/div898/strd/nls/data/LINKS/DATA/Rat43.dat

void solve_ratkowsky_fitting( derivative const & method );

void auto_ratkowsky_fitting();
void analytic_ratkowsky_fitting();

const int ratko_nbr_obs = 2;

//const double ratko_ini_b1 = 100.;
//const double ratko_ini_b2 =  10.;
//const double ratko_ini_b3 =   1.;
//const double ratko_ini_b4 =   1.;

const double ratko_ini_b1 = 700.   ;
const double ratko_ini_b2 =   5.   ;
const double ratko_ini_b3 =   0.75 ;
const double ratko_ini_b4 =   1.3  ;

const double ratko_data[] = {
     1.0e+00,     16.08e+00,
     2.0e+00,     33.83e+00,
     3.0e+00,     65.80e+00,
     4.0e+00,     97.20e+00,
     5.0e+00,    191.55e+00,
     6.0e+00,    326.20e+00,
     7.0e+00,    386.87e+00,
     8.0e+00,    520.53e+00,
     9.0e+00,    590.03e+00,
    10.0e+00,    651.92e+00,
    11.0e+00,    724.93e+00,
    12.0e+00,    699.56e+00,
    13.0e+00,    689.96e+00,
    14.0e+00,    637.56e+00,
    15.0e+00,    717.41e+00,
};


#endif // RATKOWSKY3_H
