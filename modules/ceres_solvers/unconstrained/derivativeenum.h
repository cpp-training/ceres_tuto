#ifndef DERIVATIVEENUM_H
#define DERIVATIVEENUM_H

enum class derivative { analytic, automatic, numeric };

#endif // DERIVATIVEENUM_H
