#include "easy_peasy_factory.h"


void minimize_easy_peasy( derivative const & method )
{
    // The variable to solve for with its initial value.
    double initial_x = 5.0;
    double x = initial_x;

    auto problem_ptr = EasyPeasyFactory::createEasyPeasyModel(
        method, &x);

    // Run the solver!
    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_QR;
    options.minimizer_progress_to_stdout = true;
    ceres::Solver::Summary summary;
    ceres::Solve(options, problem_ptr.get(), &summary);

    std::cout << summary.BriefReport() << "\n";
    std::cout << "x : " << initial_x
              << " -> " << x << "\n";

}



