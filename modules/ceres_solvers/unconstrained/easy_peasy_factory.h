#ifndef EASY_PEASY_FACTORY_H
#define EASY_PEASY_FACTORY_H

#include "easy_peasy.h"
#include "easypeasycostfunction.h"

#include "derivativeenum.h"


class EasyPeasyFactory
{
public:
    static std::unique_ptr<ceres::Problem> createEasyPeasyModel(
        derivative const & method, double * x )
    {
        std::unique_ptr<ceres::Problem> problemPtr = nullptr;

        if (method == derivative::automatic)
        {
            problemPtr = std::unique_ptr<ceres::Problem>(new ceres::Problem());

            // Set up the only cost function (also known as residual). This uses
            // auto-differentiation to obtain the derivative (jacobian).
            ceres::CostFunction* cost_function =
                new ceres::AutoDiffCostFunction<CostFunctor, 1, 1>(new CostFunctor);

            problemPtr->AddResidualBlock(cost_function, NULL, x);

            std::cout << "AUTOMATIC DIFFERENTIATION" << std::endl ;
        }

        if (method == derivative::analytic)
        {
            problemPtr = std::unique_ptr<ceres::Problem>(new ceres::Problem());

            ceres::CostFunction* cost_function = new EasyPeasyCostFunction{};

            problemPtr->AddResidualBlock(cost_function, NULL, x);

            std::cout << "ANALYTIC DIFFERENTIATION" << std::endl ;
        }

        return problemPtr;
    }
};


#endif // EASY_PEASY_FACTORY_H
