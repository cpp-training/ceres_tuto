
#include <ceres/ceres.h>

#include "rosenbrock.h"



void minimize_rosenbrock( derivative const & method )
{
    switch(method)
    {
    case derivative::automatic:
        std::cout << "-- AUTOMATIC --" << std::endl ;
        auto_rosenbrock_minimisation();
        break;

    case derivative::analytic:
        std::cout << "-- ANALYTIC --" << std::endl ;
        analytic_rosenbrock_minimisation();
        break;
    }

}


/**
 * @brief The Rosenbrock class
 *
 * https://en.wikipedia.org/wiki/Rosenbrock_function
 *
 * f(x, y) = (a - x)**2 + b*(y - x**2)**2
 *
 *  a = 1.
 *  b = 100.
 *
 *  The parameteters we are minimizing: x, y
 *
 */
class Rosenbrock : public ceres::FirstOrderFunction {
public:
    virtual bool Evaluate(const double* parameters,
                          double* cost,
                          double* gradient) const {
        const double x = parameters[0];
        const double y = parameters[1];

        cost[0] = (1.0 - x) * (1.0 - x) + 100.0 * (y - x * x) * (y - x * x);
        if (gradient != NULL) {
            gradient[0] = -2.0 * (1.0 - x) - 200.0 * (y - x * x) * 2.0 * x;
            gradient[1] = 200.0 * (y - x * x);
        }
        return true;
    }

    virtual int NumParameters() const { return 2; }
};


void analytic_rosenbrock_minimisation()
{
    double parameters[2] = {-1.2, 1.0};

    ceres::GradientProblem problem(new Rosenbrock());

    ceres::GradientProblemSolver::Options options;
    options.minimizer_progress_to_stdout = true;
    ceres::GradientProblemSolver::Summary summary;
    ceres::Solve(options, problem, parameters, &summary);

    std::cout << summary.FullReport() << "\n";

}


void auto_rosenbrock_minimisation()
{

}
