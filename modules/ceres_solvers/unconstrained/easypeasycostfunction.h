#ifndef EASYPEASYCOSTFUNCTION_H
#define EASYPEASYCOSTFUNCTION_H

// we could derive from: CostFunction
//
// SizedCostFunction<1, 1> is more accurate as we
// know parameters dimensions
class EasyPeasyCostFunction : public ceres::SizedCostFunction<1, 1>
{
public:
    virtual ~EasyPeasyCostFunction() {}
    virtual bool Evaluate(double const* const* parameters,
                          double* residuals,
                          double** jacobians) const {
        const double x = parameters[0][0];
        residuals[0] = 10 - x;

        // Compute the Jacobian if asked for.
        if (jacobians != NULL && jacobians[0] != NULL) {
            jacobians[0][0] = -1;
        }
        return true;
    }
};

#endif // EASYPEASYCOSTFUNCTION_H
