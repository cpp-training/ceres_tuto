#ifndef EASY_PEASY_H
#define EASY_PEASY_H

#include <ceres/ceres.h>

#include "derivativeenum.h"

void minimize_easy_peasy( derivative const & method );


struct CostFunctor {
    template <typename T>
    bool operator()(const T* const x, T* residual) const {
        residual[0] = T(10.0) - x[0];
        return true;
    }
};


#endif // EASY_PEASY_H
