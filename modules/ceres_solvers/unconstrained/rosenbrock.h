#ifndef ROSENBROCK_H
#define ROSENBROCK_H

#include "derivativeenum.h"

void minimize_rosenbrock( derivative const & method );

void auto_rosenbrock_minimisation();
void analytic_rosenbrock_minimisation();


#endif // ROSENBROCK_H
