
#include <ceres/ceres.h>

#include "curve_fitting.h"


using ceres::AutoDiffCostFunction;
using ceres::CostFunction;
using ceres::Problem;
using ceres::Solver;


void solve_curve_fitting( derivative const & method )
{
    switch(method)
    {
        case derivative::automatic:
            std::cout << "-- AUTOMATIC --" << std::endl ;
            auto_curve_fitting();
            break;

        case derivative::analytic:
            std::cout << "-- ANALYTIC --" << std::endl ;
            analytic_curve_fitting();
            break;
    }

}

/**
 * @brief The ExpoCurveCost class
 * Cost function is f(x, y) = y - exp(m*x +c)
 *
 * We fit parameters m, c using data points (x_i, y_i)
 * This is an optimisation problem depending on
 * m and c
 *
 * dimension of residual: 1
 * dimension of m: 1
 * dimension of c: 1
 *
 */
struct ExponentialResidual {
    ExponentialResidual(double x, double y)
        : x_(x), y_(y) {}
    template <typename T> bool operator()(const T* const m,
                    const T* const c,
                    T* residual) const {
        residual[0] = y_ - exp(m[0] * x_ + c[0]);
        return true;
    }
private:
    const double x_;
    const double y_;
};

void auto_curve_fitting()
{
    double m = 0.0;
    double c = 0.0;

    Problem problem;

    for (int i = 0; i < kNumObservations; ++i) {
        problem.AddResidualBlock(
            new AutoDiffCostFunction<ExponentialResidual, 1, 1, 1>(
                new ExponentialResidual(data[2 * i], data[2 * i + 1])),
            NULL,
            &m, &c);
    }

    Solver::Options options;
    options.max_num_iterations = 25;
    options.linear_solver_type = ceres::DENSE_QR;
    options.minimizer_progress_to_stdout = true;
    Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);

    std::cout << summary.BriefReport() << "\n";
    std::cout << "Initial m: " << 0.0 << " c: " << 0.0 << "\n";
    std::cout << "Final   m: " << m << " c: " << c << "\n";

}



/**
 * @brief The ExpoCurveCost class
 * Cost function is f(x, y) = y - exp(m*x +c)
 *
 * We fit parameters m, c using data points (x_i, y_i)
 * This is an optimisation problem depending on
 * m and c
 *
 * We fit m, c such that 
 * Sum_i (y_i - exp(m*x_i + c))^2 is minimum
 *
 *
 * Therefore the gradient descent depends on
 * variables m and c
 *
 * dimension of residual: 1
 * dimension of m: 1
 * dimension of c: 1
 *
 * the gradient has the following form
 * partial f/partial m = -x * exp(m*x + c)
 * partial f/partial c = - exp(m*x + c)
 *
 */
class ExpoCurveCost : public ceres::SizedCostFunction<1,1,1>
{
public:
    ExpoCurveCost(double x, double y)
        : x_(x), y_(y) {}

    virtual ~ExpoCurveCost() {}
    virtual bool Evaluate(double const* const* parameters,
                          double* residuals,
                          double** jacobians) const {
        const double m = parameters[0][0];
        const double c = parameters[1][0];

        residuals[0] = y_ - exp(m * x_ + c);

        // Compute the Jacobian if asked for.
        if (jacobians != NULL && jacobians[0] != NULL) {
            // define the gradient
            // i.e. line 0 of the jacobians
            jacobians[0][0] = - x_ * exp(m * x_ + c);
            jacobians[0][1] = - exp(m * x_ + c);
        }
        return true;
    }
private:
    const double x_;
    const double y_;
};


void analytic_curve_fitting()
{
    double m = 0.0;
    double c = 0.0;

    Problem problem;

    for (int i = 0; i < kNumObservations; ++i)
    {
        ceres::CostFunction* cost_function =
            new ExpoCurveCost{data[2 * i], data[2 * i + 1]};

        problem.AddResidualBlock(cost_function, NULL, &m, &c);
    }

    Solver::Options options;
    options.max_num_iterations = 25;
    options.linear_solver_type = ceres::DENSE_QR;
    options.minimizer_progress_to_stdout = true;
    Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);

    std::cout << summary.BriefReport() << "\n";
    std::cout << "Initial m: " << 0.0 << " c: " << 0.0 << "\n";
    std::cout << "Final   m: " << m << " c: " << c << "\n";

}


