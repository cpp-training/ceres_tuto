
#include "ratkowsky3.h"

#include "derivativeenum.h"


using ceres::AutoDiffCostFunction;
using ceres::CostFunction;
using ceres::Problem;
using ceres::Solver;

// Problem data is there
// https://www.itl.nist.gov/div898/strd/nls/data/LINKS/DATA/Rat43.dat

void solve_ratkowsky_fitting( derivative const & method )
{
    switch(method)
    {
    case derivative::automatic:
        std::cout << "-- AUTOMATIC --" << std::endl ;
        auto_ratkowsky_fitting();
        break;

    case derivative::analytic:
        std::cout << "-- ANALYTIC --" << std::endl ;
        analytic_ratkowsky_fitting();
        break;
    }

}

// https://www.itl.nist.gov/div898/strd/nls/data/ratkowsky3.shtml
struct Rat43CostFunctor_variadic {
    Rat43CostFunctor_variadic(const double x, const double y) : x_(x), y_(y) {}

    template <typename T>
    bool operator()(const T * parameters, T* residuals) const {
        const T & b1 = parameters[0];
        const T & b2 = parameters[1];
        const T & b3 = parameters[2];
        const T & b4 = parameters[3];
        residuals[0] = b1 * pow(1.0 + exp(b2 -  b3 * x_), -1.0 / b4) - y_;
        return true;
    }

private:
    const double x_;
    const double y_;
};

struct Rat43CostFunctor {
    Rat43CostFunctor(const double x, const double y) : x_(x), y_(y) {}

    template <typename T>
    bool operator()(const T* const b1, const T* const b2,
                    const T* const b3, const T* const b4,
                    T* residuals) const {
        residuals[0] = b1[0] * pow(1.0 + exp(b2[0] -  b3[0] * x_), -1.0 / b4[0]) - y_;

        return true;
    }

private:
    const double x_;
    const double y_;
};



void auto_ratkowsky_fitting()
{
    // The parameters we want to fit
    double b1 = ratko_ini_b1 ;
    double b2 = ratko_ini_b2 ;
    double b3 = ratko_ini_b3 ;
    double b4 = ratko_ini_b4 ;

    Problem problem;

    // IT WORKS :-)
//    for (int i = 0; i < ratko_nbr_obs; ++i) {
//        problem.AddResidualBlock(
//            new AutoDiffCostFunction<Rat43CostFunctor, 1, 1, 1, 1, 1>(
//                new Rat43CostFunctor(ratko_data[2 * i],
//                                     ratko_data[2 * i + 1])),
//            NULL,
//            &b1, &b2, &b3, &b4);
//    }

    // IT ALSO WORKS :-)
//    const std::vector<double*> parameters{&b1, &b2,&b3,&b4};
//    for (int i = 0; i < ratko_nbr_obs; ++i) {
//        problem.AddResidualBlock(
//            new AutoDiffCostFunction<Rat43CostFunctor, 1, 1, 1, 1, 1>(
//                new Rat43CostFunctor(ratko_data[2 * i],
//                                     ratko_data[2 * i + 1])),
//            nullptr,
//            parameters);
//    }

    // FINALLY, IT ALSO WORKS :-)
    double parameters[4] = {b1, b2,b3,b4};
    for (int i = 0; i < ratko_nbr_obs; ++i) {
        problem.AddResidualBlock(
            new AutoDiffCostFunction<Rat43CostFunctor_variadic, 1, 4>(
                new Rat43CostFunctor_variadic(ratko_data[2 * i],
                                              ratko_data[2 * i + 1])),
            nullptr,
            parameters);
    }

    Solver::Options options;
    options.max_num_iterations = 500;
    options.linear_solver_type = ceres::DENSE_QR;
    options.inner_iteration_tolerance = 1.e-6;
    options.minimizer_progress_to_stdout = true;
    Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);

    // needed with variant
    // AutoDiffCostFunction<Rat43CostFunctor_variadic, 1, 4>
    std::vector<double*> final_params{&b1, &b2, &b3, &b4};
    problem.GetParameterBlocks(&final_params);

    std::cout << summary.BriefReport() << "\n";
    std::cout << "Initial " << std::endl
              << "b1: " <<  ratko_ini_b1 << std::endl
              << "b2: " <<  ratko_ini_b2 << std::endl
              << "b3: " <<  ratko_ini_b3 << std::endl
              << "b4: " <<  ratko_ini_b4 << std::endl ;

    std::cout << "Final " << std::endl
              << "b1: " <<  final_params[0][0] << std::endl
              << "b2: " <<  final_params[0][1] << std::endl
              << "b3: " <<  final_params[0][2] << std::endl
              << "b4: " <<  final_params[0][3] << std::endl ;

//    std::cout << "Final " << std::endl
//              << "b1: " <<  b1 << std::endl
//              << "b2: " <<  b2 << std::endl
//              << "b3: " <<  b3 << std::endl
//              << "b4: " <<  b4 << std::endl ;

}



class RatkowskyAnalytic : public ceres::SizedCostFunction<1,1,1,1,1> {
public:
    RatkowskyAnalytic(const double x, const double y) : x_(x), y_(y) {}
    virtual ~RatkowskyAnalytic() {}
    virtual bool Evaluate(double const* const* parameters,
                          double* residuals,
                          double** jacobians) const {
        const double b1 = parameters[0][0];
        const double b2 = parameters[0][1];
        const double b3 = parameters[0][2];
        const double b4 = parameters[0][3];

        residuals[0] = b1 *  pow(1 + exp(b2 -  b3 * x_), -1.0 / b4) - y_;

        if (!jacobians) return true;
        double* jacobian = jacobians[0];
        if (!jacobian) return true;

        jacobian[0] = pow(1 + exp(b2 - b3 * x_), -1.0 / b4);
        jacobian[1] = -b1 * exp(b2 - b3 * x_) *
            pow(1 + exp(b2 - b3 * x_), -1.0 / b4 - 1) / b4;
        jacobian[2] = x_ * b1 * exp(b2 - b3 * x_) *
            pow(1 + exp(b2 - b3 * x_), -1.0 / b4 - 1) / b4;
        jacobian[3] = b1 * log(1 + exp(b2 - b3 * x_)) *
            pow(1 + exp(b2 - b3 * x_), -1.0 / b4) / (b4 * b4);
        return true;
    }

private:
    const double x_;
    const double y_;
};


void analytic_ratkowsky_fitting()
{
    // The parameters we want to fit
    double b1 = ratko_ini_b1 ;
    double b2 = ratko_ini_b2 ;
    double b3 = ratko_ini_b3 ;
    double b4 = ratko_ini_b4 ;

    Problem problem;

    for (int i = 0; i < ratko_nbr_obs; ++i)
    {
        ceres::CostFunction* cost_function =
            new RatkowskyAnalytic{ratko_data[2 * i],
                                  ratko_data[2 * i + 1]};

        problem.AddResidualBlock(cost_function, NULL, &b1, &b2, &b3, &b4);
    }

    Solver::Options options;
    options.max_num_iterations = 500;
    options.linear_solver_type = ceres::DENSE_QR;
    options.inner_iteration_tolerance = 1.e-6;
    options.minimizer_progress_to_stdout = true;
    Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);

    std::cout << summary.BriefReport() << "\n";
    std::cout << "Initial " << std::endl
              << "b1: " <<  ratko_ini_b1 << std::endl
              << "b2: " <<  ratko_ini_b2 << std::endl
              << "b3: " <<  ratko_ini_b3 << std::endl
              << "b4: " <<  ratko_ini_b4 << std::endl ;

    std::cout << "Final " << std::endl
              << "b1: " <<  b1 << std::endl
              << "b2: " <<  b2 << std::endl
              << "b3: " <<  b3 << std::endl
              << "b4: " <<  b4 << std::endl ;
}

//class RatkowskyAnalytic : public ceres::SizedCostFunction<1,4> {
//public:
//    RatkowskyAnalytic(const double x, const double y) : x_(x), y_(y) {}
//    virtual ~RatkowskyAnalytic() {}
//    virtual bool Evaluate(double const* const* parameters,
//                          double* residuals,
//                          double** jacobians) const {
//        const double b1 = parameters[0][0];
//        const double b2 = parameters[0][1];
//        const double b3 = parameters[0][2];
//        const double b4 = parameters[0][3];

//        residuals[0] = b1 *  pow(1 + exp(b2 -  b3 * x_), -1.0 / b4) - y_;

//        if (!jacobians) return true;
//        double* jacobian = jacobians[0];
//        if (!jacobian) return true;

//        jacobian[0] = pow(1 + exp(b2 - b3 * x_), -1.0 / b4);
//        jacobian[1] = -b1 * exp(b2 - b3 * x_) *
//            pow(1 + exp(b2 - b3 * x_), -1.0 / b4 - 1) / b4;
//        jacobian[2] = x_ * b1 * exp(b2 - b3 * x_) *
//            pow(1 + exp(b2 - b3 * x_), -1.0 / b4 - 1) / b4;
//        jacobian[3] = b1 * log(1 + exp(b2 - b3 * x_)) *
//            pow(1 + exp(b2 - b3 * x_), -1.0 / b4) / (b4 * b4);
//        return true;
//    }

//private:
//    const double x_;
//    const double y_;
//};
