from conans import ConanFile, tools, CMake


class CeresBasicsConan(ConanFile):
    name = "ceres_tuto"
    version = "0.1.0"
    author = "Mathieu Drouin (drouin.mathieu@gmail.com)"
    license = "GPL-3.0-only"
    url = "https://gitlab.com/cpp-training/ceres_tuto"
    scm = {
        "type": "git",
        "url": "auto",
        "revision": "auto",
    }
    description = (
        "Using Ceres Solver API"
        "based on http://ceres-solver.org/nnls_tutorial.html"
    )
    settings = "os", "compiler", "build_type", "arch"
    requires = ("ceres-solver/1.14.0",)
    options = {"shared": [True, False], "build_tests": [True, False]}
    default_options = {
        "shared": False,
        "build_tests": True,
        "gtest:shared": True,
        "gtest:build_gmock": False,
    }
    generators = "cmake", "virtualrunenv"

    def configure(self):
        tools.check_min_cppstd(self, "17")

    def build_requirements(self):
        if self.options.build_tests:
            self.build_requires("gtest/[^1.10]", force_host_context=True)

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.configure(defs={"CERES_BASICS_BUILD_TESTS": self.options.build_tests})
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()
        if tools.get_env("CONAN_RUN_TESTS", True):
            cmake.test()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["ceres_basics"]
